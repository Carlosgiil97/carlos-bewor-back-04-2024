<?php

namespace Tests\Vocces\Company\Infrastructure;

use Illuminate\Support\Collection;
use Vocces\Company\Domain\Company;
use App\Models\Company as ModelsCompany;

use Vocces\Company\Domain\CompanyRepositoryInterface;

use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodCreate = false;
    public bool $callMethodUpdate = false;
    public bool $callMethodFindById = false;
    public bool $callMethodAll = false;


    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
    }

    public function update(Company $company): void
    {
        $this->callMethodUpdate = true;
    }

    public function findById(CompanyId $companyId): ?Company
    {
        $this->callMethodFindById = true;
    }

    public function all(): Collection
    {
        $this->callMethodAll = true;
    }
}