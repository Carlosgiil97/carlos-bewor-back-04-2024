<?php

namespace Vocces\Company\Application;

use Illuminate\Database\Eloquent\Collection;
use Vocces\Company\Domain\CompanyRepositoryInterface;


class CompanyList
{


    /**
     * @var CompanyRepositoryInterface
     */
    private CompanyRepositoryInterface $repository;

    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function handle(): Collection
    {
        // Obtener todas las compañías del repositorio
        return $this->repository->all();
    }
}
