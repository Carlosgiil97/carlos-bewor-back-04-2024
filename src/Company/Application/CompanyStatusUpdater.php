<?php

namespace Vocces\Company\Application;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;

use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

use Illuminate\Database\Eloquent\ModelNotFoundException;


class CompanyStatusUpdater implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private CompanyRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Activate a company if it is currently inactive.
     *
     * @param string $companyId ID company.
     * @return \Vocces\Company\Domain\Company The updated company.
     * @throws \ModelNotFoundException If the company is not found.
     * @throws \InvalidArgumentException If the current status is not "inactive".
     */

    public function handle(string $companyId): Company
    {
        $company = $this->repository->findById(new CompanyId($companyId));

        if (!$company) {
            throw new ModelNotFoundException("Company not found");
        }

        if ($company->status() == CompanyStatus::enabled()) {
            throw new \InvalidArgumentException("This company is already active");
        }



        $company->setStatus(CompanyStatus::enabled());

        $this->repository->update($company);


        return $company;
    }
}