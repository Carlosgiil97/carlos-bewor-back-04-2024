<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Illuminate\Database\Eloquent\Collection;





class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::Create([
            'id'     => $company->id(),
            'name'   => $company->name(),
            'status' => $company->status(),
            'address' => $company->address(),
            'email' => $company->email()
        ]);
    }

    public function findById(CompanyId $companyId): ?Company
    {
        $model = ModelsCompany::find($companyId);

        if ($model) {
            return new Company(
                new CompanyId($model->id),
                new CompanyName($model->name),
                new CompanyStatus($model->status),
                new CompanyAddress($model->address),
                new CompanyEmail($model->email)
            );
        }

        return null;
    }

    public function update(Company $company): void
    {
        $eloquentCompany = ModelsCompany::find($company->id());

        if ($eloquentCompany) {

            $eloquentCompany->name = $company->name()->get();
            $eloquentCompany->status = $company->status();
            $eloquentCompany->address = $company->address()->get();
            $eloquentCompany->email = $company->email()->get();
            $eloquentCompany->save();
        } else {
            throw new \Exception("No se pudo encontrar la compañía con el ID especificado.");
        }
    }

    public function all(): Collection
    {
        $companies = ModelsCompany::all();
        return $companies;
    }
}