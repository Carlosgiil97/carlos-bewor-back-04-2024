<?php

namespace Vocces\Company\Domain;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Illuminate\Support\Collection;



interface CompanyRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return void
     */
    public function create(Company $company): void;

    public function findById(CompanyId $companyId): ?Company;

    public function update(Company $company): void;

    public function all(): Collection;
}