<?php

namespace Vocces\Company\Domain;

use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Vocces\Shared\Infrastructure\Interfaces\Arrayable;

final class Company implements Arrayable
{
    /**
     * @var \Vocces\Company\Domain\ValueObject\CompanyId
     */
    private CompanyId $id;

    /**
     * @var \Vocces\Company\Domain\ValueObject\CompanyName
     */
    private CompanyName $name;

    /**
     * @var \Vocces\Company\Domain\ValueObject\CompanyStatus
     */
    private CompanyStatus $status;

    /**
     * @var \Vocces\Company\Domain\ValueObject\CompanyAddress
     */
    private CompanyAddress $address;

    /**
     * @var \Vocces\Company\Domain\ValueObject\CompanyEmail
     */
    private CompanyEmail $email;



    public function __construct(
        CompanyId $id,
        CompanyName $name,
        CompanyStatus $status,
        CompanyAddress $address,
        CompanyEmail $email
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->status = $status;
        $this->address = $address;
        $this->email = $email;
    }

    public function id(): CompanyId
    {
        return $this->id;
    }

    public function name(): CompanyName
    {
        return $this->name;
    }

    public function status(): CompanyStatus
    {
        return $this->status;
    }

    public function address(): CompanyAddress
    {
        return $this->address;
    }

    public function email(): CompanyEmail
    {
        return $this->email;
    }

    public function setStatus(CompanyStatus $status)
    {
        $this->status = $status;
    }



    public function toArray()
    {
        return [
            'id'        => $this->id()->get(),
            'name'      => $this->name()->get(),
            'status'    => $this->status()->name(),
            'address'   => $this->address()->get(),
            'email'     => $this->email()->get(),
        ];
    }
}