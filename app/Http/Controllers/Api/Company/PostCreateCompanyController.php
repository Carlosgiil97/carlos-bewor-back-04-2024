<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyCreator;
use App\Http\Requests\Company\CreateCompanyRequest;

class PostCreateCompanyController extends Controller
{

    /**
     * @OA\Post(
     *      path="/api/company",
     *      operationId="companyCreator",
     *      tags={"Company"},
     *      summary="Create a new company",
     *      description="Create a new company",
     *      operationId="createCompany",
     *      @OA\RequestBody(
     *          required=true,
     *          description="Company data",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  description="Company name",
     *                  example="TechCorp"
     *              ),
     *              @OA\Property(
     *                  property="address",
     *                  type="string",
     *                  description="Company address",
     *                  example="123 Tech Street, Tech City"
     *              ),
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  description="Company email",
     *                  example="info@techcorp.com"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Company created successfully",
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Invalid request or server error"
     *      )
     * )
     */

    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\CreateCompanyRequest $request
     */
    public function __invoke(CreateCompanyRequest $request, CompanyCreator $service)
    {

        DB::beginTransaction();
        try {
            $company = $service->handle(Str::uuid(), $request->name, $request->address, $request->email);
            DB::commit();
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
