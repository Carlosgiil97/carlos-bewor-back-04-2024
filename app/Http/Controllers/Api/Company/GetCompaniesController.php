<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyList;
use Illuminate\Http\Response;



class GetCompaniesController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/companies",
     *     summary="List all companies",
     *  *     operationId="getCompaniesList",
     * tags={"Company"},
     *     @OA\Response(
     *         response=200,
     *         description="List of companies."
     *     )
     * )
     */



    public function __invoke(CompanyList $companyList)
    {

        $companies = $companyList->handle();

        return response()->json($companies, Response::HTTP_OK);
    }
}
