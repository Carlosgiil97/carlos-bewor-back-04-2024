<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\UpdateCompanyStatusRequest;
use Vocces\Company\Application\CompanyStatusUpdater;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use InvalidArgumentException;


class UpdateCompanyStatusController extends Controller
{
    /**
     * Update company to active
     * @param \App\Http\Requests\UpdateCompanyStatusRequest $request
     * @param \Vocces\Company\Application\CompanyStatusUpdater $statusUpdater
     * 
     * @return \Illuminate\Http\JsonResponse
     */

    public function __invoke(UpdateCompanyStatusRequest $request, CompanyStatusUpdater $statusUpdater, string $companyId)
    {

        DB::beginTransaction();
        try {
            $company = $statusUpdater->handle($companyId);
            DB::commit();
            return response()->json($company->toArray(), Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return response()->json(['error' => 'Company not found'], Response::HTTP_NOT_FOUND);
        } catch (InvalidArgumentException $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}